// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faCamera,
  faVideo,
  faLink,
  faPencilAlt
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import { store } from './store/store'

Vue.config.productionTip = false

// http requests config
Vue.use(VueResource)

library.add(faCamera, faVideo, faLink, faPencilAlt)
Vue.component('font-awesome-icon', FontAwesomeIcon)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})

import Vue from 'vue'
import Vuex from 'vuex'
import json from '../json/queston1.json'
Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    items: [],
    value: null,
    loading: true
  },
  getters: {
    _getList: state => {
      return state.items
    },
    _get: state => {
      return state.value
    },
    _loaded: state => {
      return state.loading
    }
  },
  mutations: {
    _setListMutation: (state, payload) => {
      state.items = payload
    },
    _setMutation: (state, payload) => {
      state.value = payload
    },
    _setLoadingMutation: (state, payload) => {
      state.loading = payload
    }
  },
  actions: {
    _setListAction: (context) => {
      if (context.state.items.length) return
      context.commit('_setListMutation', json)
      context.commit('_setLoadingMutation', false)
    },
    _loadingAction: (context, loading) => {
      context.commit('_setLoadingMutation', loading)
    }
  }
})
